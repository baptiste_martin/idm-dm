package fr.istic.m2.idm.dm;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jooq.DSLContext;

import fr.istic.m2.idm.dm.jooqData.tables.Address;
import fr.istic.m2.idm.dm.jooqData.tables.User;
import fr.istic.m2.idm.dm.jooqData.tables.records.AddressRecord;

public class Main {
	private static final Logger logger = Logger.getLogger(Main.class);
	
	public static final String LOGIN_BDD = "root";
	public static final String PASSWORD_BDD = "";
	public static final String URL_BDD = "jdbc:mysql://localhost:3306";
	public static final String NAME_BDD = "DM_IDM";
	public static final String URL_DATABASE = URL_BDD + '/' + NAME_BDD;
	
	public static void main(String[] args) {
		PropertyConfigurator.configure("./src/main/resources/log4j.properties");
		
		InitDB.initDataBase();
		
		DSLContext ctx = DBUtil.getContext();

		if (logger.isDebugEnabled())
			logger.debug("Insert address : '12 rue des fleurs'");
		
		int idAddress = insertAddress(ctx, "12 rue des fleurs").getAId();

		if (logger.isDebugEnabled())
			logger.debug("Address : '12 rue des fleurs', as inserted by id " + idAddress);
		

		if (logger.isDebugEnabled())
			logger.debug("Insert address : 'toto1'");
		
		insertUser(ctx, "toto1", "toto", idAddress);

		if (logger.isDebugEnabled())
			logger.debug("User : 'toto1', as inserted");

		if (logger.isDebugEnabled())
			logger.debug("Insert address : 'toto2'");
		
		insertUser(ctx, "toto2", "toto", idAddress);

		if (logger.isDebugEnabled())
			logger.debug("User : 'toto2', as inserted");

		if (logger.isDebugEnabled())
			logger.debug("Select user : 'toto1'");
		
		selectUser(ctx, "toto1");

		if (logger.isDebugEnabled())
			logger.debug("Update password for User 'toto1'");
		
		updateUser(ctx, "toto1", "mdp");

		if (logger.isDebugEnabled())
			logger.debug("Password update");
		
		if (logger.isDebugEnabled())
			logger.debug("Delete User 'toto2'");
		
		deleteUser(ctx, "toto2");

		if (logger.isDebugEnabled())
			logger.debug("User deleted");
	}
	
	private static AddressRecord insertAddress(DSLContext ctx, String addressUser) {
		return ctx.insertInto(Address.ADDRESS, Address.ADDRESS.ADDRESS_).values(addressUser)
			.returning(Address.ADDRESS.A_ID).fetchOne();
	}
	
	private static void insertUser(DSLContext ctx, String login, String password, int addressId) {
		ctx.insertInto(User.USER, User.USER.LOGIN, User.USER.PASSWORD, User.USER.ADDRESS)
			.values(login, password, addressId).execute();
	}
	
	private static void selectUser(DSLContext ctx, String login) {
		ctx.select().from(User.USER).where(User.USER.LOGIN.equal(login)).execute();
	}
	
	private static void updateUser(DSLContext ctx, String login, String mdp) {
		ctx.update(User.USER).set(User.USER.PASSWORD, mdp).where(User.USER.LOGIN.equal(login)).execute();
	}
	
	private static void deleteUser(DSLContext ctx, String login) {
		ctx.delete(User.USER).where(User.USER.LOGIN.equal(login)).execute();
	}
}

//	Generation des class de Jooq
//	if (logger.isDebugEnabled()) 
//		logger.debug("Initialise jooq.");
//	
//	try {
//		GenerationTool.main(new String[]{"jooq.xml"});
//	} catch (Exception e) {
//		logger.error(e.getMessage(), e);
//	}
//	
//	if (logger.isDebugEnabled()) 
//		logger.debug("Jooq initialised.");