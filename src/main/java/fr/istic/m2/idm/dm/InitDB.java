package fr.istic.m2.idm.dm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class InitDB {
	private static final Logger logger = Logger.getLogger(InitDB.class);
	
	public static void initDataBase() {
		Connection connection = null;

		try {
			if (logger.isDebugEnabled())
				logger.debug("Start connection to MySQL...");
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			connection = DriverManager.getConnection(Main.URL_BDD, Main.LOGIN_BDD, Main.PASSWORD_BDD);

			if (logger.isDebugEnabled())
				logger.debug("Connection to MySQL started...");
			
			dropDataBase(connection);
			
			createDataBase(connection);
			
			if (logger.isDebugEnabled())
				logger.debug("Start connection to database DM_IDM...");

			connection = DriverManager.getConnection(Main.URL_DATABASE, Main.LOGIN_BDD, Main.PASSWORD_BDD);

			if (logger.isDebugEnabled())
				logger.debug("Connection to database DM_IDM started...");
			
			createTable(connection);			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}
	
	private static void dropDataBase(Connection cnx) throws SQLException {
		if (logger.isDebugEnabled())
			logger.debug("Create database MySQL named " + Main.NAME_BDD + ".");
		
		try {
			Statement stmt = cnx.createStatement();
	
			stmt.executeUpdate("drop database " + Main.NAME_BDD);
		} catch (Exception e) {
			logger.info("Database is already created.");
		}
		
		if (logger.isDebugEnabled())
			logger.debug("Database created.");
	}

	private static void createDataBase(Connection cnx) throws SQLException {
		if (logger.isDebugEnabled())
			logger.debug("Create database MySQL named " + Main.NAME_BDD + ".");
		
		try {
			Statement stmt = cnx.createStatement();
	
			stmt.executeUpdate("create database " + Main.NAME_BDD);
		} catch (Exception e) {
			logger.info("Database is already created.");
		}
		
		if (logger.isDebugEnabled())
			logger.debug("Database created.");
	}
	
	private static void createTable(Connection cnx) throws SQLException {
		if (logger.isDebugEnabled())
			logger.debug("Create tables to database " + Main.NAME_BDD + ".");
		
		Statement stmt = cnx.createStatement();
		final String req1 = "CREATE TABLE Address (A_Id int not null auto_increment PRIMARY KEY, address VARCHAR(255));";

		if (logger.isDebugEnabled())
			logger.debug("Execute " + req1 + ".");
		
		try {
			stmt.executeUpdate(req1);
		} catch (Exception e) {
			logger.info("Table is already created.");
		}
		
		final String req2 = "CREATE TABLE User (U_Id int not null auto_increment PRIMARY KEY, login VARCHAR(255), password VARCHAR(255), address int, FOREIGN KEY (address) REFERENCES Address(A_Id));";

		if (logger.isDebugEnabled())
			logger.debug("Execute " + req2 + ".");
		
		try {
			stmt.executeUpdate(req2);
		} catch (Exception e) {
			logger.info("Table is already created.");
		}
		
		if (logger.isDebugEnabled())
			logger.debug("Tables created to database " + Main.NAME_BDD + ".");
	}
}
