/**
 * This class is generated by jOOQ
 */
package fr.istic.m2.idm.dm.jooqData.tables.records;

/**
 * This class is generated by jOOQ.
 */
@javax.annotation.Generated(value    = { "http://www.jooq.org", "3.4.2" },
                            comments = "This class is generated by jOOQ")
@java.lang.SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserRecord extends org.jooq.impl.UpdatableRecordImpl<fr.istic.m2.idm.dm.jooqData.tables.records.UserRecord> implements org.jooq.Record4<java.lang.Integer, java.lang.String, java.lang.String, java.lang.Integer> {

	private static final long serialVersionUID = 268403371;

	/**
	 * Setter for <code>DM_IDM.User.U_Id</code>.
	 */
	public void setUId(java.lang.Integer value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>DM_IDM.User.U_Id</code>.
	 */
	public java.lang.Integer getUId() {
		return (java.lang.Integer) getValue(0);
	}

	/**
	 * Setter for <code>DM_IDM.User.login</code>.
	 */
	public void setLogin(java.lang.String value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>DM_IDM.User.login</code>.
	 */
	public java.lang.String getLogin() {
		return (java.lang.String) getValue(1);
	}

	/**
	 * Setter for <code>DM_IDM.User.password</code>.
	 */
	public void setPassword(java.lang.String value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>DM_IDM.User.password</code>.
	 */
	public java.lang.String getPassword() {
		return (java.lang.String) getValue(2);
	}

	/**
	 * Setter for <code>DM_IDM.User.address</code>.
	 */
	public void setAddress(java.lang.Integer value) {
		setValue(3, value);
	}

	/**
	 * Getter for <code>DM_IDM.User.address</code>.
	 */
	public java.lang.Integer getAddress() {
		return (java.lang.Integer) getValue(3);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Record1<java.lang.Integer> key() {
		return (org.jooq.Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record4 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Row4<java.lang.Integer, java.lang.String, java.lang.String, java.lang.Integer> fieldsRow() {
		return (org.jooq.Row4) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Row4<java.lang.Integer, java.lang.String, java.lang.String, java.lang.Integer> valuesRow() {
		return (org.jooq.Row4) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	public org.jooq.Field<java.lang.Integer> field1() {
		return fr.istic.m2.idm.dm.jooqData.tables.User.USER.U_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	public org.jooq.Field<java.lang.String> field2() {
		return fr.istic.m2.idm.dm.jooqData.tables.User.USER.LOGIN;
	}

	/**
	 * {@inheritDoc}
	 */
	public org.jooq.Field<java.lang.String> field3() {
		return fr.istic.m2.idm.dm.jooqData.tables.User.USER.PASSWORD;
	}

	/**
	 * {@inheritDoc}
	 */
	public org.jooq.Field<java.lang.Integer> field4() {
		return fr.istic.m2.idm.dm.jooqData.tables.User.USER.ADDRESS;
	}

	/**
	 * {@inheritDoc}
	 */
	public java.lang.Integer value1() {
		return getUId();
	}

	/**
	 * {@inheritDoc}
	 */
	public java.lang.String value2() {
		return getLogin();
	}

	/**
	 * {@inheritDoc}
	 */
	public java.lang.String value3() {
		return getPassword();
	}

	/**
	 * {@inheritDoc}
	 */
	public java.lang.Integer value4() {
		return getAddress();
	}

	/**
	 * {@inheritDoc}
	 */
	public UserRecord value1(java.lang.Integer value) {
		setUId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	public UserRecord value2(java.lang.String value) {
		setLogin(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	public UserRecord value3(java.lang.String value) {
		setPassword(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	public UserRecord value4(java.lang.Integer value) {
		setAddress(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	public UserRecord values(java.lang.Integer value1, java.lang.String value2, java.lang.String value3, java.lang.Integer value4) {
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached UserRecord
	 */
	public UserRecord() {
		super(fr.istic.m2.idm.dm.jooqData.tables.User.USER);
	}

	/**
	 * Create a detached, initialised UserRecord
	 */
	public UserRecord(java.lang.Integer uId, java.lang.String login, java.lang.String password, java.lang.Integer address) {
		super(fr.istic.m2.idm.dm.jooqData.tables.User.USER);

		setValue(0, uId);
		setValue(1, login);
		setValue(2, password);
		setValue(3, address);
	}
}
