package fr.istic.m2.idm.dm;

import java.sql.Connection;
import java.sql.DriverManager;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

public class DBUtil {
	public static DSLContext getContext() {
		Connection conn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(Main.URL_DATABASE, Main.LOGIN_BDD, Main.PASSWORD_BDD);

    		return DSL.using(conn, SQLDialect.MYSQL);    		
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}
}
